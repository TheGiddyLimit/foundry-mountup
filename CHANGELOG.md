# 2.0.1
- Ensure compatibility with 0.5.6

# 2.0
- Significant rewrite to how ride links are stored
	- No longer requires sockets or game settings
	- Should eliminate multiple issues experienced due to permission issues

# 1.2.1
- Fixes the text for the settings hints
- Fixed the error message during module init

# 1.2
- Fixed permission issues by handling ride creation on GM side via socket
- Better handling of ancestor interactions (you should now longer be able to have a rider be mounted by its own mount)

# 1.1
- Fixed dismount chat message sending mount as rider and rider as mount

# 1.0
- Initial Release!