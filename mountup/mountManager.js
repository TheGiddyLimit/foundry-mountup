import {Chatter} from "./chatter.js";
import {getTokenById, flagScope, flag} from "./utils.js";

/**
 * Provides all of the functionality for interacting with the game (tokens, canvas, etc.)
 */
export class MountManager {

	/**
	 * Called when the mount up button was clicked on a token's HUD
	 * Determines if conditions are appropriate for mounting, and executes the mount if so
	 * @param {Object} data - The token from which the button was clicked on the hud
	 */
	static async doMountUp (data) {
		const selectedTokens = canvas.tokens.controlled;

		if (selectedTokens.some(tk => this.isMountInUseBy(data._id, tk.id))) {
			await this.doDismountAll(data);
			return true;
		}

		if (selectedTokens.length === 0) {
			ui.notifications("Please select the token to be the rider first.");
			return false;
		} else if (selectedTokens.some(it => it.id === data._id)) {
			ui.notifications("You can't mount yourself.");
			return false;
		} else {
			for (const target of selectedTokens) await this._doMountUp_doMount(data, target);
			return true;
		}
	}

	static async _doMountUp_doMount (data, target) {
		const rider = getTokenById(target.id);
		const mount = getTokenById(data._id);

		const existingRiders = mount.getFlag(flagScope, flag.Riders);
		const nxtRiders = [...(existingRiders || []), rider.id];

		await mount.setFlag(flagScope, flag.Riders, nxtRiders);
		await rider.setFlag(flagScope, flag.Mount, mount.id);
		await rider.setFlag(flagScope, flag.OrigSize, {w: rider.w, h: rider.h});

		// Refresh rider positions
		for (let i = 0; i < nxtRiders.length; ++i) {
			const rider = getTokenById(nxtRiders[i]);
			await this.doMoveRiderToMount(rider, mount, nxtRiders.length, i);
		}

		canvas.getLayer("TokenLayer").hud.render();

		Chatter.pDoSendMountMessage(target.id, data._id);
	}

	static async doDismount (mountId, riderId) {
		const rider = getTokenById(riderId);
		const mount = getTokenById(mountId);

		await this.doRestoreRiderSize(rider, mount);
		await rider.unsetFlag(flagScope, flag.Mount);

		const riders = mount.getFlag(flagScope, flag.Riders);
		const nxtRiders = (riders || []).filter(id => id !== rider.data._id);
		await mount.setFlag(flagScope, flag.Riders, nxtRiders);

		canvas.getLayer("TokenLayer").hud.render();

		Chatter.pDoSendDismountMessage(rider.data._id, mount.data._id);
	}

	static async doDismountAll (mountData) {
		const mount = getTokenById(mountData._id);
		const riders = (mount.getFlag(flagScope, flag.Riders) || []).map(riderId => getTokenById(riderId));

		for (const rider of riders) await this.doRestoreRiderSize(rider, mount);

		for (const rider of riders) {
			await rider.unsetFlag(flagScope, flag.Mount);
			Chatter.pDoSendDismountMessage(rider.data._id, mountData._id);
		}

		canvas.getLayer("TokenLayer").hud.render();

		await mount.unsetFlag(flagScope, flag.Riders);
	}

	/**
	 * Restores the size of a mount's rider token to original size
	 * @param {Object} rider - The rider who needs to be restored
	 * @param {Object} mount - The mount. Only used for positioning the dismounted rider.
	 */
	static async doRestoreRiderSize (rider, mount) {
		const originalSize = rider.getFlag(flagScope, flag.OrigSize);

		if (rider.w < originalSize.w || rider.h < originalSize.h) {
			const grid = canvas.scene.data.grid;
			const newWidth = rider.w < originalSize.w ? originalSize.w : rider.w;
			const newHeight = rider.h < originalSize.h ? originalSize.h : rider.H;

			await rider.update({
				width: newWidth / grid,
				height: newHeight / grid
			});
		}

		await rider.update({
			x: mount.x,
			y: mount.y,
		});

		rider.parent.sortChildren();

		await rider.unsetFlag(flagScope, flag.OrigSize);
	}

	/**
	 * Called when a token is deleted, checks if the token is part of any ride link, and breaks said link
	 * @param {Object} tokenId - The token being deleted
	 */
	static async handleTokenDelete (tokenId) {
		const token = getTokenById(tokenId);

		if (this.isRider(token.id)) {
			const mount = getTokenById(token.getFlag(flagScope, flag.Mount));
			await mount.unsetFlag(flagScope, flag.Riders);
		}

		if (this.isMountInUse(token.id)) {
			const riderIds = token.getFlag(flagScope, flag.Riders);

			for (const riderId of riderIds) {
				const rider = getTokenById(riderId);
				await rider.unsetFlag(flagScope, flag.Mount);
				await rider.unsetFlag(flagScope, flag.OrigSize);
			}
		}
	}

	static doPopAllRiders () {
		canvas.tokens.placeables.forEach((token) => {
			if (this.isMountInUse(token.id) && !this.isRider(token.id)) {
				this.doPopRiders(token.id);
			}
		});
	}

	static async doPopRiders (mountId) {
		const mount = getTokenById(mountId);

		const riderIds = mount.getFlag(flagScope, flag.Riders) || [];

		for (const riderId of riderIds) {
			const rider = getTokenById(riderId);

			if (rider) {
				rider.zIndex = mount.zIndex + 10;
			}

			if (this.isMountInUse(rider.id)) {
				await this.doPopRiders(rider.id);
			}
		}

		mount.parent.sortChildren();

		canvas.getLayer("TokenLayer").hud.render();
	}

	/**
	 * Called when a token is moved in the game.
	 * Determines if the token being moved is a mount - if it is, moves the rider to match
	 * @param {String} tokenId - The ID of the token being moved
	 * @param {Object} updateData - Update data being sent by the game
	 */
	static async handleTokenMovement (tokenId, updateData) {
		if (this.isMountInUse(tokenId)) {
			const mount = getTokenById(tokenId);

			// A mount moved, make the rider follow
			const riderIds = mount.getFlag(flagScope, flag.Riders)
			for (let i = 0; i < riderIds.length; ++i) {
				const rider = getTokenById(riderIds[i]);
				await this.doMoveRiderToMount(rider, mount, riderIds.length, i, updateData.x, updateData.y);
			}
		}
	}

	/**
	 * Returns true if the token is currently serving as a mount in any existing ride link
	 * @param {String} tokenId - The ID of the token to evaluate
	 */
	static isMountInUse (tokenId) {
		const token = getTokenById(tokenId);
		if (!token) return false;

		return token.getFlag(flagScope, flag.Riders) != null && !!token.getFlag(flagScope, flag.Riders).length;
	}

	static isMountInUseBy (mountId, riderId) {
		const mount = getTokenById(mountId);
		if (!mount) return false;
		const riders = mount.getFlag(flagScope, flag.Riders) || [];
		return riders.some(id => id === riderId);
	}

	static isBelowMountLimit (tokenId) {
		const token = getTokenById(tokenId);
		if (!token) return false;

		const riders = token.getFlag(flagScope, flag.Riders);
		const maxRiders = token.data.height * token.data.height;
		return riders == null || riders.length < maxRiders;
	}

	/**
	 * Returns true if the token is currenty serving as a rider in any existing ride link
	 * @param {String} tokenId - The ID of the token to evaluate
	 */
	static isRider (tokenId) {
		const token = getTokenById(tokenId);
		if (!token) return false

		return token.getFlag('mountup', 'mount') != null;
	}

	static isRidersMount (mountId, riderId) {
		const rider = getTokenById(riderId);
		const mount = getTokenById(mountId);
		return (rider.getFlag(flagScope, flag.Mount) === mount.id);
	}

	/**
	 * Moves the Rider token to Mount token.
	 * If both tokens are being moved together, newX and newY must be provided, or rider
	 *  will end up at the Mount's starting location
	 * @param {Object} rider - The rider
	 * @param {Object} mount - The mount
	 * @param {Number} riderCount - The total number of riders
	 * @param {Number} ixRider - The index of this rider
	 * @param {Number} newX - (optional) The new X-coordinate for the move
	 * @param {Number} newY - (optional) The new Y-coordinate for the move
	 */
	static async doMoveRiderToMount (rider, mount, riderCount, ixRider, newX = undefined, newY = undefined, ) {
		if (rider.w >= mount.w || rider.h >= mount.h) {
			const grid = canvas.scene.data.grid;
			await rider.update({
				width: (mount.w / 2) / grid,
				height: (mount.h / 2) / grid,
			});
			rider.zIndex = mount.zIndex + 10;
		}

		const mountPos = {
			x: newX == null ? mount.x : newX,
			y: newY == null ? mount.y : newY
		};
		const mountCenter = mount.getCenter(mountPos.x, mountPos.y);

		// For a single rider, place it in the X-Y centre of the token
		if (riderCount === 1) {
			await rider.update({
				x: mountCenter.x - (rider.w / 2),
				y: mountCenter.y - (rider.h / 2),
			});
		}
		// for a pair of riders, place them in the X-centre of the token
		else if (riderCount === 2) {
			await rider.update({
				x: mountCenter.x - (rider.w / 2),
				y: ixRider === 0 ? mountCenter.y - rider.h : mountCenter.y,
			});
		}
		// Otherwise, tile the riders
		else {
			// TODO(Future) this may literally be the worst possible implementation of this idea
			for (let col = 0; col < mount.data.width; ++col) {
				for (let row = 0; row < mount.data.height; ++row) {

					const n = (row * mount.data.height) + col;
					if (n === ixRider) {
						await rider.update({
							x: mountPos.x + (col * rider.w),
							y: mountPos.y + (row * rider.h),
						});

						break;
					}
				}
			}
		}

		rider.zIndex = mount.zIndex + 10;

		rider.parent.sortChildren();
	}

	static isAncestor (childId, ancestorId) {
		if (!this.isRider(childId)) return false;

		const child = getTokenById(childId);
		const parent = getTokenById(child.getFlag(flagScope, flag.Mount));
		if (parent.id === ancestorId) return true;
		return this.isAncestor(parent.id, ancestorId);
	}
}

