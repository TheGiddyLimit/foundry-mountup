import {MountManager} from "./mountManager.js";
import {Settings} from "./settings.js";
import {flag, flagScope} from "./utils.js";

/**
 * Functionality class for managing the token HUD
 * TODO refresh the hud on mount/dismount
 */
export class MountHud {

	/**
	 * Called when a token is right clicked on to display the HUD.
	 * Adds a button with a horse icon.
	 * @param {Object} app - the application data
	 * @param {Object} html - the html data
	 * @param {Object} token - The HUD Data
	 */
	static renderMountHud (app, html, token) {

		const allSelected = canvas.tokens.controlled;
		const selected = allSelected[0];

		// if exactly 0 selected and hud is from any mount then show the dismount button
		if (allSelected.length === 0 && MountManager.isMountInUse(token._id)) {
			this.addDismountAllButton(html, token);
		}
		// if exactly 1 is selected
		else if (allSelected.length === 1) {
			// if the hud is from the same as selection
			if (selected.id === token._id) {
				// AND it is a mount
				if (MountManager.isMountInUse(selected.id)) {
					this.addDismountAllButton(html, token);
				}
			}
			// If our token is riding the selected token
			else if (MountManager.isRidersMount(token._id, selected.id)) {
				this.addDismountButton(html, token._id, selected.id);
			}
			// If the selected token is riding our token
			else if (MountManager.isRidersMount(selected.id, token._id)) {
				this.addDismountButton(html, selected.id, token._id);
			}
			// if the hud is NOT a mount, or if the token is below its mount limit
			else if (!MountManager.isMountInUse(token._id) || MountManager.isBelowMountLimit(token._id)) {
				// AND the selected is NOT a rider - show the mount button
				if (!MountManager.isRider(selected.id) && !MountManager.isAncestor(token._id, selected.id)) {
					this.addMountButton(html, token);
				}
			}
		}
	}

	static addMountButton (html, data) {
		const button = $(`<div class="control-icon mount-up"><i class="fas ${Settings.getIcon()}"></i></div>`);

		html.find('.col.left').prepend(button);
		button.find('i').click(() => MountManager.doMountUp(data));
	}

	static addDismountAllButton (html, data) {
		const $button = $(`<div class="control-icon mount-up fa-stack">
			<i class="fas ${Settings.getIcon()} fa-stack-1x"></i>
			<i class="fas fa-slash fa-stack-1x" style="position: absolute; color: tomato"></i>
		</div>`)
			.click(() => MountManager.doDismountAll(data));

		html.find('.col.left').prepend($button);
	}

	static renderRiderHud (app, html, data) {
		if (!MountManager.isRider(data._id)) return;

		const mountId = data.flags[flagScope][flag.Mount];

		return this.addDismountButton(html, mountId, data._id);
	}

	static addDismountButton (html, mountId, riderId) {
		const $button = $(`<div class="control-icon mount-up fa-stack">
			<i class="fas ${Settings.getIcon()} fa-stack-1x"></i>
			<i class="fas fa-slash fa-stack-1x" style="position: absolute; color: tomato"></i>
		</div>`)
			.click(() => MountManager.doDismount(mountId, riderId));

		html.find('.col.left').prepend($button);
	}
}

