import {Settings} from "./settings.js";
import {getTokenById} from "./utils.js";

export class Chatter {

	/**
	 * Attempts to display a chat message notifying about a mount action
	 * @param {String} riderId - The ID of the rider
	 * @param {String} mountId - The ID of the mount
	 */
	static async pDoSendMountMessage (riderId, mountId) {
		if (Settings.shouldChat()) {
			const icon = `<span class="fa-stack"><i class="fas ${Settings.getIcon()} fa-stack-1x"></i></span>&nbsp;`;
			this.pDoSendChatMessage(icon + Settings.getMountMessage(), riderId, mountId);
		}
	}

	/**
	 * Attempts to display a chat message notifying about a dismount action
	 * @param {String} riderId - The ID of the rider
	 * @param {String} mountId - The ID of the mount
	 */
	static async pDoSendDismountMessage (riderId, mountId) {
		if (Settings.shouldChat()) {
			const icon = `<span class="fa-stack" >
                            <i class="fas ${Settings.getIcon()} fa-stack-1x"></i>
                            <i class="fas fa-slash fa-stack-1x" style="color: tomato"></i>
                        </span>&nbsp;`;
			await this.pDoSendChatMessage(icon + Settings.getDismountMessage(), riderId, mountId);
		}
	}

	static async pDoSendChatMessage (message, riderId, mountId) {
		const rider = getTokenById(riderId);
		const mount = getTokenById(mountId);

		message = message.replace("{mount}", mount.data.name).replace("{rider}", rider.data.name);

		await ChatMessage.create({
			content: message
		});
	}

}
