export const flagScope = 'mountup';

export const flag = {
	Mount: 'mount',
	Riders: 'riders',
	OrigSize: 'origsize'
};

/**
 * Returns a token object from the canvas based on the ID value
 * @param {String} tokenId - The ID of the token to look for
 */
export function getTokenById (tokenId) {
	return canvas.tokens.placeables.find(t => t.id === tokenId);
}
