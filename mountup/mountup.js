import {Settings} from "./settings.js";
import {MountHud} from "./mountHud.js";
import {MountManager} from "./mountManager.js";

Hooks.on('ready', () => {
	Settings.registerSettings();
});

Hooks.on('canvasReady', () => {
	MountManager.doPopAllRiders();
});

Hooks.on('renderTokenHUD', (app, html, data) => {
	MountHud.renderMountHud(app, html, data);
	MountHud.renderRiderHud(app, html, data);
});

Hooks.on('preUpdateToken', async (scene, token, updateData) => {
	if (updateData.x || updateData.y) {
		await MountManager.handleTokenMovement(token._id, updateData);
	}
});

Hooks.on('updateToken', async (scene, token, updateData) => {
	if (MountManager.isMountInUse(updateData._id)) {
		await MountManager.doPopRiders(updateData._id);
	}
});

Hooks.on('controlToken', async (token) => {
	if (MountManager.isMountInUse(token.id)) {
		await MountManager.doPopRiders(token.id);
	}
});

Hooks.on('preDeleteToken', async (scene, token) => {
	await MountManager.handleTokenDelete(token._id);
	return true;
});
